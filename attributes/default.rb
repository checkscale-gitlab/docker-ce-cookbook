# frozen_string_literal: true

# APT Docker package version to be installed
default[:docker_ce][:version] = 'latest'

# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
# Docker deamon.json file options
# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
#
# => registry-mirrors (Array of String)
#
# It replaces the daemon registry mirrors with a new set of registry mirrors.
# If some existing registry mirrors in daemon's configuration are not in
# newly reloaded registry mirrors, these existing ones will be removed
# from daemon's config.
default[:docker_ce][:daemon][:registry_mirrors] = []
#
# => exec-opts native.cgroupdriver (String)
#
# Can only be "cgroupds" or "systemd".
#
# Changes the Docker control groups (cgroups).
# A cgroup limits an application to a specific set of resources.
# Control groups allow Docker Engine to share available hardware resources to
# containers and optionally enforce limits and constraints.
# For example, you can limit the memory available to a specific container.
# Source: https://docs.docker.com/engine/docker-overview/
#
# This parameter is used in order to update the Docker daemon.json file as the
# following:
#
# cat > /etc/docker/daemon.json <<EOF
# {
#   "exec-opts": ["native.cgroupdriver=<cgroup_driver>"],
#   ...
# }
# EOF
#
# This will solve the following warning:
#
#   detected "cgroupfs" as the Docker cgroup driver. The recommended driver is
#   "systemd". Please follow the guide at https://kubernetes.io/docs/setup/cri/
default[:docker_ce][:daemon][:cgroup_driver] = 'cgroupfs'
#
# By default, when the Docker daemon terminates, it shuts down running
# containers. Starting with Docker Engine 1.12, you can configure the daemon so
# that containers remain running if the daemon becomes unavailable.
# This functionality is called live restore.
# The live restore option helps reduce container downtime due to daemon crashes,
# planned outages, or upgrades.
#
# default setting is false
#
default[:docker_ce][:daemon][:live_restore] = false
#
# To configure the Docker daemon to default to a specific logging driver,
# set the value of log-driver to the name of the logging driver in the
# daemon.json file, which is located in /etc/docker/ on Linux hosts.
# The default logging driver is json-file.
#
default[:docker_ce][:daemon][:log_driver] = 'json-file'
#
# To configure the docker0 network interface MTU (Maximum Transmission Unit),
# chanfe the following attribute.
# The default is nil, meaning not defined in the /etc/docker/daemon.json.
#
default[:docker_ce][:daemon][:mtu] = nil
