# Docker CE Cookbook

Installs the Docker engine Community Edition.

This cookbook has been built following the instructions from https://docs.docker.com/install/.

This cookbook takes care to lock the docker packages after install and upgrade in order to prevent any unexpected actions on the package. It also takes care to never restart Docker excepted when upgrading it.

## Another Docker engine cookbook?

All the existing cookbooks I found on [the Chef Supermarket](http://supermarket.chef.io) wasn't covering my need, or wasn't working anymore, so I decided to make a new one, with tests, following the official documentation so that it should be more robust.

## Supported Platforms

* Debian 8/9/10
* Ubuntu 18.04 (bionic), 20.04 (focal)

_If you want to add more platforms, feel free to open a PR!_

## Attributes

Please note that all attributes are optional.

| Key | Type | Description | Default |
|-----|------|-------------|---------|
| `default[:docker_ce][:version]` | String | APT package version to be installed | `latest` |
| `node[:docker_ce][:daemon][:registry_mirrors]` | Array of strings | Docker registry mirrors | `[]` |
| `node[:docker_ce][:daemon][:cgroup_driver]` | String | Cgroup driver to be used by Docker | `cgroupfs` |
| `node[:docker_ce][:daemon][:live_restore]` | Boolean | Enables/Disables the Docker live restore feature | `false` |
| `node[:docker_ce][:daemon][:log_driver]` | String | Configures the Docker daemon logging driver | `log_driver` |

## Usage

 - If you use it from a Policyfile, a Role or an Environment, add it to the
`run_list` :

  ```yaml
  {
    "run_list": [
      "recipe[docker-ce]"
    ]
  }
  ```

 - If you want to use it within a cookbook :
  ```ruby
  include_recipe 'docker-ce'
  ```

## Tested Docker versions

*Note*: If you'd like to test another Docker serie, or another platform, please update the `.kitchen.yml` file as the following:

1. Duplicate an existing version from the `suites` section or add the new platform from the `platforms` section
2. Update the docker version to the version you'd like to test
3. Run the tests (See [`TESTING.md`](TESTING.md))
4. Update the `README.md` file with the tested version
5. When tests are passing, open a merge request

### Debian

|              | 18.09 | 18.06 |
| ------------ | ----- | ----- |
| buster64     | ✅    | ✅   |
| stretch64    | ✅    | ✅   |
| jessie64     | ✅    | ✅   |

### Ubuntu

|              | 18.09 | 18.06 |
| ------------ | ----- | ----- |
| ubuntu 18.04 | ✅    | ✅   |
